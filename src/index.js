import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
//import Demo from './Demo';  // nhớ rằng cái import phải là chữ viết hoa mới được. chữ nào cũng dc vì đã export default
                            // hoặc có 1 cách nữa là theo ES6 export {tên component} và import {tên component} và cũng cần 
                            // phải đặt tên component là chữ viết hoa

import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
