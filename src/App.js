import React, { Component } from 'react';
import './App.css';
import Nav from './components/Nav'
import Form from './components/Form'
import List from'./components/List'
import { Badge } from 'reactstrap';

class App extends Component {
  render() {
    return (
      <div>
        <Nav />
        <Form />
        <div className="list">
          <h6>Danh sách đơn hàng <Badge color="danger">New update</Badge></h6>
          <List />
        </div>
      </div>
    );
  }
}

export default App;
