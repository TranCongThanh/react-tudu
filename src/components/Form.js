import React, {Component} from 'react'
import { Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import './css/Form.css'
class form extends Component {
    render () {
        return (
            <Form>
                <Row form>
                <Col md={6}>
                    <FormGroup>
                    <Label for="exampleEmail">Tên</Label>
                    <Input type="name" name="name" id="exampleName" placeholder="Họ và tên ... " />
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                    <Label for="exampleAddress">Địa chỉ</Label>
                    <Input type="address" name="address" id="exampleAddress" placeholder="Nhập vào địa chỉ ..." />
                    </FormGroup>
                </Col>
                </Row>
                <Row form>
                    <Col md={6}>
                        <FormGroup>
                        <Label for="examplePhone">Điện thoại</Label>
                        <Input type="phone" name="phone" id="examplePhone" placeholder="Nhập vào số điện thoại ..."/>
                        </FormGroup>
                    </Col>
                    <Col md={6}>
                        <FormGroup>
                        <Label for="exampleEmail">Email</Label>
                        <Input type="email" name="email" id="exampleEmail" placeholder="Nhập vào email ... "/>
                        </FormGroup>
                    </Col>
                </Row>
                <Row form>
                    <Col md={4}>
                        <FormGroup>
                        <Label for="exampleProduct">Tên sản phẩn</Label>
                        <Input type="product" name="product" id="exampleProduct" placeholder="Nhập vào tên sản phẩm ..."/>
                        </FormGroup>
                    </Col>
                    <Col md={4}>
                        <FormGroup>
                        <Label for="exampleAmong">Số lượng</Label>
                        <Input type="among" name="among" id="exampleAmong" placeholder="Nhập vào số lượng sản phẩm ..."/>
                        </FormGroup>
                    </Col>
                    <Col md={4}>
                        <FormGroup>
                        <Label for="exampleCode">Code</Label>
                        <Input type="code" name="code" id="exampleCode" placeholder="Nhập vào mã giảm giá ..."/>
                        </FormGroup>  
                    </Col>
                </Row>
                <FormGroup check>
                <Input type="checkbox" name="check" id="exampleCheck"/>
                <Label for="exampleCheck" check>Giao hàng nhanh</Label>
                </FormGroup>
                <Button className="button" outline color="danger">Thêm</Button>
            </Form>
        );
    }
} 
export default form;