import React, {Component} from 'react'
import { Table } from 'reactstrap';
import './css/List.css'
import Pagination from './Pagination'
class list extends Component {
    render () {
        return (
            <Table bordered>
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Địa chỉ</th>
                    <th>Điện Thoại</th>
                    <th>Email</th>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Trạng Thái</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    <td>@mdo</td>
                    <td>@mdo</td>
                    <td>@mdo</td>
                    <td>@mdo</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                    <td>@fat</td>
                    <td>@fat</td>
                    <td>@fat</td>
                    <td>@fat</td>
                </tr>
                </tbody>
                <Pagination></Pagination>
            </Table>
        );
    }
}
export default list;